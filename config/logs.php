<?php

return [
    /*
   |--------------------------------------------------------------------------
   | LogRotate parameter
   |--------------------------------------------------------------------------
   |
   | The duration in days that we will keep the logs in our storage.
   |
   */
    'database_retention_time' => 14,

    /*
    |--------------------------------------------------------------------------
    | LogRotate parameter
    |--------------------------------------------------------------------------
    |
    | The maximum size in megabytes allowed for a log file. This parameter is useful to avoid server saturation
    |
    */
    'max_log_file_size' => 14,

    /*
   |--------------------------------------------------------------------------
   | Exchange TAG
   |--------------------------------------------------------------------------
   |
   | If tag is present, the request will be logged.
   |
   */
    'tag' => 'must_be_logged',

    /*
    |--------------------------------------------------------------------------
    | Storage path
    |--------------------------------------------------------------------------
    |
    */
    'storage_path' => 'logs',

    /*
    |--------------------------------------------------------------------------
    |  Request Exclusions
    |--------------------------------------------------------------------------
    |
    | This sets which request data is excluded from the logging
    |
    */
    'dont_log' => [
        'password',
        'password_confirmation',
        'new_password',
        'old_password',
    ],

    /*
    |--------------------------------------------------------------------------
    |  Max payload size authorized
    |--------------------------------------------------------------------------
    |
    | This sets maximum size authorized for a client payload in KB
    |
    */
    'max_payload_size' => 500,

    /*
   |--------------------------------------------------------------------------
   |  Log levels
   |--------------------------------------------------------------------------
   |
   | This is an array with all the available log levels
   |
   */
    'logs_levels' => ['info', 'success', 'warning', 'error', 'emergency'],

    /*
   |--------------------------------------------------------------------------
   |  Date format
   |--------------------------------------------------------------------------
   |
   */
    'date_format' => 'Y-m-d',

    /*
   |--------------------------------------------------------------------------
   |  Parameter type name
   |--------------------------------------------------------------------------
   |
   | Name of parameter passed from request to determine logs type

   */
    'parameter_type_name' => 'cronos_logs_type',
];
