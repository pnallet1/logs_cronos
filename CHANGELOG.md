<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [2.1.1](https://gitlab.com//pu-cronos/components/logs/compare/v2.1.0...v2.1.1) (2021-08-18)
---

## [2.1.0](https://gitlab.com//pu-cronos/components/logs/compare/v2.0.3...v2.1.0) (2021-08-18)
### Features

* Add parameter type name from config ([1db935](https://gitlab.com//pu-cronos/components/logs/commit/1db935f0efbd44fc6c62f073ac0cd1812520c6b3))

### Bug Fixes

* Update phpunit configuration ([35c397](https://gitlab.com//pu-cronos/components/logs/commit/35c397a709cea597afb651aa293f72e6162c7e4d))

---

## [2.0.3](https://gitlab.com//pu-cronos/components/logs/compare/v2.0.2...v2.0.3) (2021-08-16)
### Bug Fixes

* Cast paginate parameter ([db49b0](https://gitlab.com//pu-cronos/components/logs/commit/db49b0cff417d744b047322c83dca73c83d66414))

---

## [2.0.2](https://gitlab.com//pu-cronos/components/logs/compare/v2.0.1...v2.0.2) (2021-08-16)
### Bug Fixes

* Bad pagination ([62270a](https://gitlab.com//pu-cronos/components/logs/commit/62270a1fad6a059747d03f6f39b82d9a4e928d3d))

---

## [2.0.1](https://gitlab.com//pu-cronos/components/logs/compare/v2.0.0...v2.0.1) (2021-08-09)
---

## [](https://gitlab.com//pu-cronos/components/logs/compare/...v) (2021-08-02)
---

## [](https://gitlab.com//pu-cronos/components/logs/compare/...v) (2021-08-02)
---

## [1.0.0](https://gitlab.com//pu-cronos/components/logs/compare/464a0399f700d1ab1ff46e4d26c1be17bf346ec7...v1.0.0) (2021-08-02)
---

# Changelog

All notable changes to `Skeleton` will be documented in this file

## 0.1.0 - 2019-05-27

* Package initialisation
