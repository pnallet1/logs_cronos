<?php

declare(strict_types=1);

namespace Cronos\Logs\Models;

use Carbon\Carbon;
use Cronos\Logs\Contracts\PathContract;
use Illuminate\Support\Facades\File;

/**
 * Class Path
 *
 * @package Cronos\Logs\Models
 */
class Path implements PathContract
{
    public static function makeDirectory(string $type): bool
    {
        File::makeDirectory(self::getStorage($type), 0777, true, true);

        return true;
    }

    public static function getStorage(string $type): string
    {
        return storage_path(config('logs.storage_path', 'logs'))
            .DIRECTORY_SEPARATOR
            .$type;
    }

    public static function deleteFile(string $type, string $file): bool
    {
        File::delete(self::getFile($type, $file));

        return true;
    }

    public static function getFile(string $type, string $file): string
    {
        return self::getStorage($type)
            .DIRECTORY_SEPARATOR
            .$file;
    }

    public static function appendFile(string $type, string $filename, string $content): bool
    {
        File::append(self::getFile($type, $filename), $content.PHP_EOL);

        return true;
    }

    public static function directoryExists(string $type): bool
    {
        if (File::isDirectory(self::getStorage($type))) {
            return true;
        }

        return false;
    }

    public static function isValidFilename(string $filename): bool
    {
        if (preg_match('/.-(.*).log/', $filename, $matches)) {
            return Carbon::canBeCreatedFromFormat($matches[1], config('logs.date_format', 'Y-m-d'));
        }

        return false;
    }
}
