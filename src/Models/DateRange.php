<?php

declare(strict_types=1);

namespace Cronos\Logs\Models;

use Carbon\Carbon;
use Cronos\Logs\Contracts\DateRangeContract;

/**
 * Class DateRange
 *
 * @package Cronos\Logs\Models
 */
class DateRange implements DateRangeContract
{
    /** @var Carbon|\Illuminate\Support\Carbon */
    private Carbon $start_date;

    /** @var Carbon|\Illuminate\Support\Carbon */
    private Carbon $end_date;

    /**
     * DateRange constructor.
     */
    public function __construct(?Carbon $start_date = null, ?Carbon $end_date = null)
    {
        $this->setStartDate($start_date ?? now());
        $this->setEndDate($end_date ?? now());
    }

    /**
     * Get the start_date of the date range with a given format
     */
    public function getStartDateFromFormat(string $format): string
    {
        return $this->start_date->format($format);
    }

    /**
     * Get the start_date of the date range
     */
    public function getStartDate(): Carbon
    {
        return $this->start_date;
    }

    /**
     * Set the start_date of the date range
     */
    public function setStartDate(Carbon $start_date): void
    {
        $this->start_date = $start_date;
    }

    /**
     * Get the start of day of start_date of the date range
     */
    public function getStartOfDayStartDate(): Carbon
    {
        return $this->start_date->startOfDay();
    }

    /**
     * Get the end_date of the date range with a given format
     */
    public function getEndDateFromFormat(string $format): string
    {
        return $this->end_date->format($format);
    }

    /**
     * Get the end_date of the date range
     */
    public function getEndDate(): Carbon
    {
        return $this->end_date;
    }

    /**
     * Set the end date of the date range
     */
    public function setEndDate(Carbon $end_date): void
    {
        $this->end_date = $end_date;
    }

    /**
     * Get the start of day of end_date of the date range
     */
    public function getStartOfDayEndDate(): Carbon
    {
        return $this->end_date->startOfDay();
    }
}
