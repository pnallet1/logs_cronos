<?php

declare(strict_types=1);

namespace Cronos\Logs\Abstracts;

use Carbon\Carbon;
use Cronos\Logs\Contracts\ExchangeContract;
use Cronos\Logs\Exceptions\ExchangeInvalidFilenameException;
use Cronos\Logs\Models\Path;
use Cronos\Logs\Utils\Date;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use JsonException;
use Symfony\Component\HttpFoundation\Response;

abstract class ExchangeAbstract implements ExchangeContract
{
    use Date;

    private string $type;

    /**
     * ExchangeAbstract constructor.
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @throws ExchangeInvalidFilenameException
     */
    public function get(): Collection
    {
        if (! Path::directoryExists($this->type)) {
            return collect();
        }

        return collect($this->getContent());
    }

    /**
     * @throws ExchangeInvalidFilenameException
     */
    public function delete(Carbon $date): bool
    {
        if (Path::directoryExists($this->type)) {
            foreach (preg_grep('/^([^.])/', scandir(Path::getStorage($this->type))) as $filename) {
                if (
                    is_file(Path::getFile($this->type, $filename))
                    && $date >= $this->getCarbonInstanceFromFilename($filename)
                ) {
                    Path::deleteFile($this->type, $filename);
                }
            }
        }

        return true;
    }

    /**
     * @throws JsonException
     */
    public function save(?Request $request = null, ?Response $response = null): bool
    {
        $content = implode(';', $this->generateLine($request, $response));

        Path::makeDirectory($this->type);
        Path::appendFile($this->type, $this->getFilename(), $content.PHP_EOL);

        return true;
    }

    /**
     * @throws ExchangeInvalidFilenameException
     */
    protected function getCarbonInstanceFromFilename(string $filename): Carbon
    {
        if (! Path::isValidFilename($filename)) {
            throw new ExchangeInvalidFilenameException();
        }

        return Carbon::createFromFormat(config('logs.date_format', 'Y-m-d'), substr($filename, -14, 10))->startOfDay();
    }

    /**
     * @return array
     *
     * @throws JsonException
     */
    protected function generateLine(?Request $request = null, ?Response $response = null): array
    {
        $startTime = microtime(true);
        $lines = [];
        $lines['created_at'] = Carbon::now();
        $lines['method'] = optional($request)->method();
        $lines['url'] = optional($request)->path();
        $lines['payload'] = $this->payload($request);
        $lines['response'] = optional($response)->status();
        $lines['duration'] = number_format($startTime - LARAVEL_START, 3);

        return $lines;
    }

    /**
     * @throws JsonException
     */
    protected function payload(?Request $request = null): string
    {
        $allFields = optional($request)->all() ?? [];

        // A message is rendered if the payload is too large to handle, according to the config file parameter.
        if (strlen(json_encode($allFields, JSON_THROW_ON_ERROR)) * 8 / 1024 > config('logs.max_payload_size')) {
            return json_encode(
                ['message' => 'payload size is too large, check the maximum payload size authorized in the config file'],
                JSON_THROW_ON_ERROR
            );
        }

        foreach (config('logs.dont_log', []) as $key) {
            if (array_key_exists($key, $allFields)) {
                unset($allFields[$key]);
            }
        }

        return json_encode($allFields, JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     *
     * @throws ExchangeInvalidFilenameException
     * @throws Exception
     */
    private function getContent(): array
    {
        $dateRange = $this->getDateRange();
        $content = [];
        foreach (scandir(Path::getStorage($this->type)) as $filename) {
            if (is_dir($filename)) {
                continue;
            }
            try {
                if (self::dateIsInRange(
                    $this->getCarbonInstanceFromFilename($filename),
                    $dateRange
                )) {
                    $content[] = $this->contentLines($filename);
                }
            } catch (ExchangeInvalidFilenameException $exception) {
                continue;
            } catch (Exception $exception) {
                throw new Exception($exception->getMessage());
            }
        }

        return Arr::flatten($content);
    }

    /**
     * @param $file
     *
     * @return array
     */
    private function contentLines($file): array
    {
        $content = [];
        $lines = file(Path::getFile($this->type, $file), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {
            $content[] = $this->toObject(
                preg_split('/"[^"]*"(*SKIP)(*FAIL)|;/', $line)
            );
        }

        return $content;
    }
}
