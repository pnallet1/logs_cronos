<?php

declare(strict_types=1);

namespace Cronos\Logs\Services\Exchanges;

use Carbon\Carbon;
use Cronos\Logs\Abstracts\ExchangeAbstract;
use Illuminate\Support\Arr;
use stdClass;

/**
 * Class ApiExchange
 *
 * @package Cronos\Logs\Services\Exchanges
 */
class ApiExchange extends ExchangeAbstract
{
    public const TYPE = 'api';

    protected string $date_format;

    /**
     * Exchange constructor.
     */
    public function __construct()
    {
        $this->date_format = config('logs.date_format', 'Y-m-d');

        parent::__construct(self::TYPE);
    }

    public function toObject(array $data): stdClass
    {
        $model = new stdClass();
        $model->created_at = Arr::has($data, 0) ? Carbon::make($data[0]) : null;
        $model->method = Arr::has($data, 1) ? $data[1] : null;
        $model->url = Arr::has($data, 2) ? $data[2] : null;
        $model->payload = Arr::has($data, 3) ? $data[3] : null;
        $model->response = Arr::has($data, 4) ? $data[4] : null;
        $model->duration = Arr::has($data, 5) ? $data[5] : null;

        return $model;
    }

    public function getFilename(): string
    {
        $type = self::TYPE;
        $date = date($this->date_format);

        return "${type}-${date}.log";
    }
}
