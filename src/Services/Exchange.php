<?php

declare(strict_types=1);

namespace Cronos\Logs\Services;

use Cronos\Logs\Abstracts\ExchangeAbstract;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

/**
 * Class Exchange
 *
 * @package Cronos\Logs\Services
 */
class Exchange
{
    public function __invoke(): ExchangeAbstract
    {
        return static::applyDecorators();
    }

    protected static function applyDecorators(): ExchangeAbstract
    {
        /** @var  Exchange $decorator */
        $decorator = static::createFilterDecorator(Request::get(config('parameter_type_name', 'cronos_logs_type'), 'api'));

        if (static::isValidDecorator($decorator)) {
            return new $decorator();
        }

        return abort(404);
    }

    protected static function createFilterDecorator($name): string
    {
        return 'Cronos\\Logs\\Services\\Exchanges\\'.Str::of($name)->studly().'Exchange';
    }

    protected static function isValidDecorator($decorator): bool
    {
        return class_exists($decorator);
    }
}
