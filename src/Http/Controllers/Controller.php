<?php

declare(strict_types=1);

namespace Cronos\Logs\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;

/**
 * Class Controller
 *
 * @package Cronos\Logs\Http\Controllers
 *
 * @author Antoine Tran <atran@pageup.fr>
 */
class Controller extends BaseController
{
    /**
     * Method used to paginate the given collection with the given parameters
     *
     * @param            $items
     */
    public function paginate($items, ?int $perPage = 15, ?int $page = 1): LengthAwarePaginator
    {
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            $items->count(),
            $perPage,
            $page
        );
    }
}
