<?php

declare(strict_types=1);

namespace Cronos\Logs\Http\Controllers;

use Cronos\Logs\Services\Exchange;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class LogController
 *
 * @package Cronos\Logs\Http\Controllers
 *
 * @author  Antoine Tran <atran@pageup.fr>
 */
class LogsController extends Controller
{
    /**
     * @throws BindingResolutionException
     */
    public function index(Request $request): JsonResponse
    {
        set_unlimited_memory();

        return response()->json(
            $this->paginate(
                app()->make(Exchange::class)()->get(),
                (int) $request->get('per_page', 15),
                (int) $request->get('page', 1)
            )
        );
    }

    /**
     * @throws BindingResolutionException
     */
    public function destroy(Request $request): JsonResponse
    {
        if (! $request->has('suppression_date')) {
            return response()->json(null, 204);
        }

        app()->make(Exchange::class)()->delete(create_date_from_format($request->get('suppression_date')));
        return response()->json(
            null,
            204
        );
    }
}
