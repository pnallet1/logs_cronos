<?php

declare(strict_types=1);

namespace Cronos\Logs\Http\Middleware;

use Closure;
use Cronos\Logs\Abstracts\ExchangeAbstract;
use Cronos\Logs\Services\Exchange as ExchangeService;
use Illuminate\Http\Request;

/**
 * Class Exchange
 *
 * @package Cronos\Logs\Http\Middleware
 */
class Exchange
{
    protected ExchangeAbstract $exchange;

    /** @var string|array|null */
    private $request_tag;

    /** @var string|array|null */
    private $response_tag;

    /**
     * Exchange constructor.
     */
    public function __construct(ExchangeService $exchange)
    {
        $this->exchange = ($exchange)();
    }

    /**
     * Handle an incoming request.
     *
     * @return mixed
     *
     * @throws \JsonException
     */
    public function handle(Request $request, Closure $next)
    {
        $this->request_tag = $request->header(config('logs.tag', 'must_be_logged'));

        $response = $next($request);

        $this->response_tag = $response->headers->get(config('logs.tag'));

        if ($this->shouldBeLogged()) {
            $this->exchange->save($request, $response);
        }

        return $response;
    }

    private function shouldBeLogged(): bool
    {
        return (bool) $this->response_tag || (bool) $this->request_tag;
    }
}
