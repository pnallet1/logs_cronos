<?php

declare(strict_types=1);

namespace Cronos\Logs\Providers;

use Cronos\Logs\Console\Commands\LogRotate;
use Cronos\Logs\Contracts\ExchangeContract;
use Cronos\Logs\Contracts\PathContract;
use Cronos\Logs\Models\Path;
use Cronos\Logs\Services\Exchange;
use Cronos\Logs\Services\Exchanges\ApiExchange;
use Cronos\Logs\Services\Exchanges\TaskExchange;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

/**
 * Class LogsServiceProvider
 *
 * @author Antoine TRAN <atran@pageup.fr>
 *
 * @package Cronos\Crude\Providers
 */
class LogsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../../config/logs.php' => config_path('logs.php'),
            ], 'config');
        }
        $this->mergeConfigFrom(__DIR__ . '/../../config/logs.php', 'logs');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');

        $this->app->bind(ExchangeContract::class, Exchange::class);
        $this->app->bind(PathContract::class, Path::class);

        $this->app->singleton(Exchange::class);
        $this->app->singleton(ApiExchange::class);
        $this->app->singleton(TaskExchange::class);

        $this->app->booted(function (): void {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('log:rotate')->daily();
        });
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        $this->commands([
            LogRotate::class,
        ]);

        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(\Cronos\Logs\Http\Middleware\Exchange::class);
    }
}
