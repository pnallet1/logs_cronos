<?php

declare(strict_types=1);

namespace Cronos\Logs\Console\Commands;

use Carbon\Carbon;
use Cronos\Logs\Services\Exchanges\ApiExchange;
use Cronos\Logs\Services\Exchanges\TaskExchange;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class LogRotate
 *
 * @package Cronos\Logs\Console\Commands
 */
class LogRotate extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'log:rotate';

    /**
     * The console command description.
     */
    protected $description = 'LogRotate - Delete all the log files where the conditions are met';

    /**
     * Execute the console command.
     *
     * @throws BindingResolutionException
     */
    public function handle(): bool
    {
        $this->info('Start --- '.Carbon::now());
        $this->line('');

        $this->info('Logs suppression [...]');
        $date = now()->subDays(config('logs.database_retention_time'));

        $apiLogger = app()->make(ApiExchange::class);
        $apiLogger->delete($date);

        $taskLogger = app()->make(TaskExchange::class);
        $taskLogger->delete($date);

        $this->line('');
        $this->info('Finished successful --- '.Carbon::now());

        return true;
    }
}
