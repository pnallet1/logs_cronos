<?php

declare(strict_types=1);

namespace Cronos\Logs\Contracts;

use Carbon\Carbon;

/**
 * Interface DateRangeContract
 *
 * @package Cronos\Logs\Contracts
 */
interface DateRangeContract
{
    public function getStartDateFromFormat(string $format): string;

    public function getStartDate(): Carbon;

    public function getStartOfDayStartDate(): Carbon;

    public function setStartDate(Carbon $start_date): void;

    public function getEndDateFromFormat(string $format): string;

    public function getEndDate(): Carbon;

    public function getStartOfDayEndDate(): Carbon;

    public function setEndDate(Carbon $end_date): void;
}
