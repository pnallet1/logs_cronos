<?php

declare(strict_types=1);

namespace Cronos\Logs\Contracts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use stdClass;

interface ExchangeContract
{
    public function get(): Collection;

    public function save(?Request $request = null, ?Response $response = null): bool;

    public function delete(Carbon $date): bool;

    public function toObject(array $data): stdClass;

    public function getFilename(): string;
}
