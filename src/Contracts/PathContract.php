<?php

declare(strict_types=1);

namespace Cronos\Logs\Contracts;

/**
 * Interface PathContract
 *
 * @package Cronos\Logs\Contracts
 */
interface PathContract
{
    public static function getFile(string $type, string $file): string;

    public static function getStorage(string $type): string;

    public static function makeDirectory(string $type): bool;

    public static function deleteFile(string $type, string $file): bool;

    public static function appendFile(string $type, string $filename, string $content): bool;

    public static function directoryExists(string $type): bool;
}
