<?php

declare(strict_types=1);

namespace Cronos\Logs\Exceptions;

use Exception;

/**
 * Class ExchangeInvalidFilenameException
 *
 * @package Cronos\Logs\Exceptions
 */
class ExchangeInvalidFilenameException extends Exception
{
}
