<?php

declare(strict_types=1);

namespace Cronos\Logs\Utils;

use Carbon\Carbon;
use Cronos\Logs\Models\DateRange;
use Illuminate\Support\Facades\Request;

/**
 * Trait Date
 *
 * @package Cronos\Logs\Utils
 */
trait Date
{
    private static function dateIsInRange(Carbon $date, DateRange $dateRange): bool
    {
        return $date >= $dateRange->getStartOfDayStartDate()
            && $date <= $dateRange->getStartOfDayEndDate();
    }

    private function getDateRange(): DateRange
    {
        return new DateRange(
            Request::has('start_datetime') ?
                Carbon::createFromFormat(
                    config('logs.date_format', 'Y-m-d'),
                    Request::get('start_datetime')
                ) : null,
            Request::has('end_datetime') ?
                Carbon::createFromFormat(
                    config('logs.date_format', 'Y-m-d'),
                    Request::get('end_datetime')
                ) : null,
        );
    }
}
