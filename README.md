# Logs

* [Installation](#markdown-header-installation)
  * [Laravel](#markdown-header-laravel)
* [Usage](#markdown-header-usage)
  * [LogRotate parameters](#markdown-header-log-rotate parameters)
  * [Logrotate CRON](#markdown-header-log-rotate-cron)
* [Logging Exchange](#markdown-header-test)
  * [Server side](#markdown-header-server_side)
  * [Client side](#markdown-header-client-side)
* [Logging parameters](#markdown-header-logging-parameters)
* [Logging Tasks](#markdown-header-logging-tasks)
* [API Controller Logs](#markdown-header-api-controller-logs)
  * [Pagination](#markdown-header-pagination)
* [Test](#markdown-header-test)
* [Contributing](#markdown-header-contributing)
* [Author and Contributors](#markdown-header-author)

This package allows API requests and tasks logging

## Installation

You can install the package via composer:

```json
{
    "repositories": [
        { "type": "vcs", "url": "git@gitlab.com:pu-cronos/components/logs.git" }
    ]
}
```

```
composer require cronos/logs
```

### Laravel

You have to publish the config file with this command :

```php
php artisan vendor:publish --provider="Cronos\Logs\Providers\LogsServiceProvider" --tag="config"
```

If it doesn't work for any particular reasons, use the following command :

`php artisan vendor:publish --force`

then select `LogsServiceProvider`, it will force the publication.
## Usage

Once you have installed the package, you can already log API requests.
However, you have to configure the config file `logs.php`

In this config file, you can update multiple parameters related to the module :

### LogRotate parameters

`database_retention_time` : this parameter represents the duration in days that we will keep
the logs in our storage. The default duration is 14 days

```php
'database_retention_time' => 14,
```

LogRotate parameters

`max_log_file_size` : this parameter represents the maximum size in megabytes allowed for a
log file. This parameter is useful to avoid server saturation. The default maximum size is 14 
megabytes.

```php
'max_log_file_size' => 14,
```

###Logrotate CRON

The log rotation is an automated process used in system administration in which log
files are compressed, moved (archived), renamed or deleted once they are too old or too big.
The logrotate is run as a daily cron job.

By default, the logrotate cron job will be automatically registered into your project as you
install the package through the service provider. If for some reasons, that is not the case,
you will have to do it yourself.

In order to do so, you have to update your `kernel` file with the given line in the `schedule`
method :

```php
/**
 * Define the application's command schedule.
 *
 * @param Schedule $schedule
 * @return void
 */
protected function schedule(Schedule $schedule)
{
    $schedule->command('log:rotate')->daily();
}
```

## Logging Exchange 

The logging of API requests depends on the presence of a parameter in the headers, in the server
side as well as the client side.

This parameter is called `must-be-logged` by default, but you can always change the tag in the config 
file.

```php
'tag' => 'must_be_logged',
```

###Example

###Server side

In this example, let's assume that we have built an API to manage books. Moreover, we have
an `index` method that returns all the books from the database. 


```php
public function index(): JsonResponse
{
    return response()->json(Book::all(), 200);
}
```

If you want to log this API request, you simply have to add the parameter `must-be-logged`
in the headers with the value `1`

```php
public function index(): JsonResponse
{
    return response()->json(Book::all(), 200)->header('must-be-logged', 1);
}
```

The `value` of the parameter `must_be_logged` is really important.

`1` means that the request will be logged no matter what happens

`0` means that the request will never be logged 

###Client side

We have seen how to log API requests from the server side, but we can also log 
API requests from the client side. In order to do so, you have to pass the parameter
`must_be_logged` to the headers of the request, from the client side. the parameter
must have a given value `0` or `1`

`1` means that the request will be logged if the server does not reject it 

`0` means that the request will not be logged unless the server does it anyway



Note that the parameter coming from the server `will always take precedence` 
over the parameter coming from the client. Here is an array that will give more 
details


 SERVER (must_be_logged) | CLIENT (must_be_logged)  | will-be-logged  |
:----------------------: | :----------------------: |:---------------:
 0                       | 0                        |No
 0                       | 1                        |No
 1                       | 0                        |Yes
 1                       | 1                        |Yes
 Absence                 | 0                        |No
 Absence                 | 1                        |Yes
 0                       | Absence                  |No
 1                       | Absence                  | Yes


##Logging parameters

There are multiple parameters in the config file that you can use in order to
`personalize` the logging. 

`dont_log` : this parameter is used to exclude all the fields that we don't
want to log in our application. That is pretty useful when it comes to log
some sensitive data like passwords. So `dont_log` is an array where you can put all
of the fields that you don't want to log in your application.


```php
'dont_log' => [
    'password',
    'password_confirmation',
    'new_password',
    'old_password',
],
```


`max_payload_size` : this parameter represents the maximum size of the client payload that
we will log. The payload is the part of transmitted data that is the actual intended
message. The unit of measurement is KB. By default, the maximum size is set to 500 KB.

```php
'max_payload_size' => 500,
```

##Logging Tasks

With this package, it is possible to log `tasks` and `crons`. This feature is not automatic
and you have to provide some informations to the logging. Here is how you can use the
tasks logging feature :

First you need to get into the file where the command is located. More precisely in the `"handle"` 
method, where the logic of the command is defined.

The first thing to do is to declare a `TaskExchange` entity.

```php
 /**
 * Execute the console command.
 */
public function handle()
{
    $logger = new TaskExchange();

    var_dump('Command processing, here is the logic of the command');
}
```

Then you have to pass the signature of the command to the TaskExchange's `start`'s method. It is 
also possible but optional to give a payload with useful information.

```php 
  /**
   * Execute the console command.
   */
  public function handle()
  {
  
      $logger = new TaskExchange();
      $logger->start([
          'command' => $this->signature,
          'payload' => ['some_information' => 'This information is really useful']
      ]);
  
      var_dump('Command processing, here is the logic of the command');
  }
```

Finally, when the command is over, you have to use the TaskExchange's `end`'s method.

The `first parameter` of the method is to determine whether the command succeeded 
or failed. You are the one that will determine if the command succeeded or not.
The parameter is a `boolean` type :

- `null` means that the command ended successfully


- `true` means that the command ended successfully


- `false` means that somehow the command failed

The `second parameter` of the method is an array that you can pass with information.
It is the equivalent of the `payload` from the `start` method. In other words, you can store
useful information from the ended command. Note that this parameter is also optional.

```php 
  /**
   * Execute the console command.
   */
  public function handle()
  {
  
      $logger = new TaskExchange();
      $logger->start([
          'command' => $this->signature,
          'payload' => ['some_information' => 'This information is really useful']
      ]);
  
      var_dump('Command processing, here is the logic of the command');
      
       $logger->end(true, [
            'useful_information_1' => 'hello there',
            'useful_information_2' => 'the sky is blue',
        ]);
  }
```

Furthermore, note that the `end`'s method will automatically log the end date and
the duration of the command.


##API Controller Logs

the package comes along with a dedicated `API` that you can use to retrieve the logs,
wether the api logs or the task logs.

Here is the default route for the API
`GET : .../api/logs`

In order to call this API, you have to pass `various parameters` in the body of the request.
The first two parameters corresponds to date range in which we will retrieve the logs.

- `start_datetime`: represents the  starting date of the date range
  

- `end_datetime`: represents the ending date of the date range

The module awaits a specific format for the date : `yyyy-mm-dd`. Here is an example of the
body of the request. The API will retrieve all the logs in the given date
range ( `2021-03-01` included to `2021-03-06` included ).  In the case scenario
where those parameters are absent from the body request, the API will retrieve today's logs.

```json
{
  "start_datetime": "2021-03-01",
  "end_datetime": "2021-03-06"
}
```

Besides, there is another important parameter that you will have to add to the body request.

the `type` parameter. This allows the API to know which type of logs the user wants to get.
Either the task logs, or the api logs.

- `api` gets the API logs


- `task` gets the task logs

Here is an example of a use case : 


```json
{
  "start_datetime": "2021-01-09",
  "end_datetime": "2021-03-06",
  "type": "api"
}
```


###Pagination

By default, when you will use the API to get the logs. the returned answer
will always be `paginated`. you can change this behavior by passing a boolean
parameter called `paginate` to the body request.

- `true` the response will be paginated


- `false` the response won't be paginated


- `parameter absent` the response will be paginated

As the `"paginate"` option is enabled by default, you can pass
the traditionnal pagination parameters in the body request such
as `"page"` for the wanted page or `"per_page"` for the number of items per page.


## Test

Run :

```php
./vendor/bin/phpunit
```

## Contributing

Please, before pull request executes PHP-CS-FIXER.


## Author and Contributors

* Antoine TRAN <jcharcosset@pageup.fr>
* Clément Braillon <cbraillon@pageup.fr>
* Johnny Charcosset <jcharcosset@pageup.fr>
