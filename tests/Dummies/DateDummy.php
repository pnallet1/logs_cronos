<?php

namespace Cronos\Logs\Tests\Dummies;

use Cronos\Logs\Models\DateRange;
use Cronos\Logs\Utils\Date;
use Carbon\Carbon;

/**
 * Class DateDummy
 *
 * @package Cronos\Logs\Tests\Dummies
 */
class DateDummy
{
    use Date;

    /**
     * @return DateRange
     */
    public function getDateRangeDummy(): DateRange
    {
        return $this->getDateRange();
    }

    /**
     * @param  Carbon     $date
     * @param  DateRange  $dateRange
     * @return bool
     */
    public function dateIsInRangeDummy(Carbon $date, DateRange $dateRange): bool
    {
        return self::dateIsInRange($date, $dateRange);
    }
}
