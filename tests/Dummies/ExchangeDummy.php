<?php

namespace Cronos\Logs\Tests\Dummies;

use Cronos\Logs\Abstracts\ExchangeAbstract;
use JsonException;
use stdClass;

/**
 * Class ExchangeDummy
 *
 * @package Cronos\Logs\Tests\Dummies
 */
class ExchangeDummy extends ExchangeAbstract
{
    /**
     * @throws JsonException
     */
    public function toObject(array $data): stdClass
    {
        return (object) $data;
    }

    public function getFilename(): string
    {
        return '';
    }
}
