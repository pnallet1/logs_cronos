<?php

namespace Cronos\Logs\Tests\Services\Exchanges;

use Cronos\Logs\Services\Exchanges\ApiExchange;
use Cronos\Logs\Tests\TestCase;
use stdClass;

/**
 * Class ApiExchangeTest
 *
 * @package Cronos\Logs\Tests\Services\Exchanges
 * @coversDefaultClass Cronos\Logs\Services\Exchanges
 */
class ApiExchangeTest extends TestCase
{
    /**
     * Test toObject
     *
     * @group services
     * @cover ::toObject
     * @cover ::__construct
     */
    public function testEmptyToObject(): void
    {
        $newObject = new stdClass();
        $newObject->created_at = null;
        $newObject->method = null;
        $newObject->url = null;
        $newObject->payload = null;
        $newObject->response = null;
        $newObject->duration = null;

        $exchange = new ApiExchange();
        $object = $exchange->toObject([]);

        $this->assertEquals($newObject, $object);
    }

    /**
     * Test toObject
     *
     * @group services
     * @cover ::toObject
     * @cover ::__construct
     */
    public function testToObject(): void
    {
        $date = now();

        $newObject = new stdClass();
        $newObject->created_at = $date;
        $newObject->method = 'POST';
        $newObject->url = 'http://test.app';
        $newObject->payload = ['data' => null];
        $newObject->response = ['response' => 'created'];
        $newObject->duration = 0.600;

        $exchange = new ApiExchange();
        $object = $exchange->toObject([
            $date,
            'POST',
            'http://test.app',
            ['data' => null],
            ['response' => 'created'],
            0.600,
        ]);

        $this->assertEquals($newObject, $object);
    }

    /**
     * Test getFilename
     *
     * @group services
     * @cover ::getFilename
     * @cover ::__construct
     */
    public function testGetFilename(): void
    {
        $exchange = new ApiExchange();
        $this->assertEquals('api-'.date('Y-m-d').'.log', $exchange->getFilename());
    }

    /**
     * Test getFilename
     *
     * @group services
     * @cover ::getFilename
     * @cover ::__construct
     */
    public function testGetFilenameOtherFormat(): void
    {
        config()->set('logs.date_format', 'Y');
        $exchange = new ApiExchange();

        $this->assertEquals('api-'.date('Y').'.log', $exchange->getFilename());
    }
}
