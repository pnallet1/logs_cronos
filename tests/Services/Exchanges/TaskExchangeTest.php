<?php

namespace Cronos\Logs\Tests\Services\Exchanges;

use Cronos\Logs\Services\Exchanges\ApiExchange;
use Cronos\Logs\Services\Exchanges\TaskExchange;
use Cronos\Logs\Tests\TestCase;
use stdClass;

/**
 * Class TaskExchangeTest
 *
 * @package Cronos\Logs\Tests\Services\Exchanges
 * @coversDefaultClass Cronos\Logs\Services\Exchanges
 */
class TaskExchangeTest extends TestCase
{
    /**
     * Test toObject
     *
     * @group services
     * @cover ::toObject
     * @cover ::__construct
     */
    public function testEmptyToObject(): void
    {
        $newObject = new stdClass();
        $newObject->started_at = null;
        $newObject->command = null;
        $newObject->payload = null;
        $newObject->ended_at = null;
        $newObject->duration = null;
        $newObject->status = null;

        $exchange = new TaskExchange();
        $object = $exchange->toObject([]);

        $this->assertEquals($newObject, $object);
    }
    /**
     * Test toObject
     *
     * @group services
     * @cover ::toObject
     * @cover ::__construct
     */
    public function testEmptyToObjectWithOption(): void
    {
        $now = now();
        $newObject = new stdClass();
        $newObject->started_at = $now;
        $newObject->command = null;
        $newObject->payload = null;
        $newObject->ended_at = $now;
        $newObject->duration = null;
        $newObject->status = null;
        $newObject->options = [];

        $exchange = new TaskExchange();
        $object = $exchange->toObject([
            $now,
            null,
            null,
            $now,
            null,
            null,
            [],
        ]);

        $this->assertEquals($newObject, $object);
    }

    /**
     * Test toObject
     *
     * @group services
     * @cover ::toObject
     * @cover ::__construct
     */
    public function testToObject(): void
    {
        $date = now();

        $newObject = new stdClass();
        $newObject->started_at = $date;
        $newObject->command = 'php artisan make:test';
        $newObject->payload = ['data' => null];
        $newObject->ended_at = $date;
        $newObject->duration = 0.600;
        $newObject->status = 'SUCCESS';

        $exchange = new TaskExchange();
        $object = $exchange->toObject([
            $date,
            'php artisan make:test',
            ['data' => null],
            $date,
            0.600,
            'SUCCESS',
        ]);

        $this->assertEquals($newObject, $object);
    }

    /**
     * Test getFilename
     *
     * @group services
     * @cover ::getFilename
     * @cover ::__construct
     */
    public function testGetFilename(): void
    {
        $exchange = new TaskExchange();
        $this->assertEquals('task-'.date('Y-m-d').'.log', $exchange->getFilename());
    }

    /**
     * Test getFilename
     *
     * @group services
     * @cover ::getFilename
     * @cover ::__construct
     */
    public function testGetFilenameOtherFormat(): void
    {
        config()->set('logs.date_format', 'Y');
        $exchange = new TaskExchange();

        $this->assertEquals('task-'.date('Y').'.log', $exchange->getFilename());
    }
}
