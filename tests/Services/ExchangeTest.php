<?php

namespace Cronos\Logs\Tests\Services;

use Cronos\Logs\Services\Exchange;
use Cronos\Logs\Services\Exchanges\ApiExchange;
use Cronos\Logs\Tests\TestCase;
use Illuminate\Support\Facades\Request;
use Mockery;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExchangeTest
 *
 * @package Cronos\Logs\Tests\Services
 * @coversDefaultClass Cronos\Logs\Services
 */
class ExchangeTest extends TestCase
{
    /**
     * Test invoke
     *
     * @group services
     */
    public function testInvoke(): void
    {
        $exchange = new Exchange();

        $this->assertInstanceOf(ApiExchange::class, $exchange());
    }

    /**
     * Test invoke
     *
     * @group services
     */
    public function test404Invoke(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request->shouldReceive('get')
            ->once()
            ->andReturn('NotFound');

        $this->expectException(NotFoundHttpException::class);
        (new Exchange())();
    }
}
