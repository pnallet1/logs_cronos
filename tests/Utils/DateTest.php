<?php

namespace Cronos\Logs\Tests\Utils;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Cronos\Logs\Models\DateRange;
use Cronos\Logs\Tests\Dummies\DateDummy;
use Cronos\Logs\Tests\TestCase;
use Illuminate\Support\Facades\Request;
use Mockery;

/**
 * Class DateTest
 *
 * @package Cronos\Logs\Tests\Utils
 */
class DateTest extends TestCase
{
    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithoutParam(): void
    {
        $date = new DateDummy();
        $date_range = new DateRange();

        $this->assertEquals(
            $date_range->getStartOfDayStartDate(),
            $date->getDateRangeDummy()->getStartOfDayStartDate()
        );
        $this->assertEquals($date_range->getStartOfDayEndDate(), $date->getDateRangeDummy()->getStartOfDayEndDate());
    }

    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithStartDatetimeParam(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request
            ->shouldReceive('has')
            ->with('start_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('has')
            ->with('end_datetime')
            ->andReturnFalse();
        $request
            ->shouldReceive('get')
            ->andReturn('2020-01-01');

        $date = new DateDummy();
        $date_range = new DateRange();

        $this->assertEquals(
            Carbon::create('2020', '01', '01')->startOfDay(),
            $date->getDateRangeDummy()->getStartOfDayStartDate()
        );
        $this->assertEquals($date_range->getStartOfDayEndDate(), $date->getDateRangeDummy()->getStartOfDayEndDate());
    }

    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithEndDatetimeParam(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request
            ->shouldReceive('has')
            ->with('end_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('get')
            ->with('end_datetime')
            ->andReturn('2020-01-01');
        $request
            ->shouldReceive('has')
            ->with('start_datetime')
            ->andReturnFalse();

        $date = new DateDummy();
        $date_range = new DateRange();

        $this->assertEquals($date_range->getStartOfDayStartDate(), $date->getDateRangeDummy()->getStartOfDayStartDate());
        $this->assertEquals(
            Carbon::create('2020', '01', '01')->startOfDay(),
            $date->getDateRangeDummy()->getStartOfDayEndDate()
        );
    }

    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithEndDatetimeParamAndStartDatetimeParam(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request
            ->shouldReceive('has')
            ->with('end_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('get')
            ->with('end_datetime')
            ->andReturn('2020-01-01');
        $request
            ->shouldReceive('has')
            ->with('start_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('get')
            ->with('start_datetime')
            ->andReturn('2020-01-01');

        $date = new DateDummy();

        $this->assertEquals(
            Carbon::create('2020', '01', '01')->startOfDay(),
            $date->getDateRangeDummy()->getStartOfDayStartDate()
        );
        $this->assertEquals(
            Carbon::create('2020', '01', '01')->startOfDay(),
            $date->getDateRangeDummy()->getStartOfDayEndDate()
        );
    }

    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithEndDatetimeParamButBadFormat(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request
            ->shouldReceive('has')
            ->with('end_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('get')
            ->with('end_datetime')
            ->andReturn('01 01 2021');
        $request
            ->shouldReceive('has')
            ->with('start_datetime')
            ->andReturnFalse();

        $this->expectException(InvalidFormatException::class);

        $date = new DateDummy();
        $date->getDateRangeDummy()->getStartOfDayStartDate();
    }

    /**
     * Test getDateRange
     *
     * @group models
     */
    public function testGetDateRangeWithStartDatetimeParamButBadFormat(): void
    {
        $request = Mockery::mock('alias:'.Request::class)->makePartial();
        $request
            ->shouldReceive('has')
            ->with('start_datetime')
            ->andReturnTrue();
        $request
            ->shouldReceive('get')
            ->with('start_datetime')
            ->andReturn('01 01 2021');
        $request
            ->shouldReceive('has')
            ->with('end_datetime')
            ->andReturnFalse();

        $this->expectException(InvalidFormatException::class);

        $date = new DateDummy();
        $date->getDateRangeDummy()->getStartOfDayStartDate();
    }

    /**
     * Test dateIsInRange
     *
     * @group models
     */
    public function testBadDateIsInRange(): void
    {
        $carbon = Carbon::createFromFormat('Y-m-d', '2020-01-01');
        $date_range = new DateRange();

        $date = new DateDummy();
        $date->dateIsInRangeDummy($carbon, $date_range);

        $this->assertFalse($date->dateIsInRangeDummy($carbon, $date_range));
    }

    /**
     * Test dateIsInRange
     *
     * @group models
     */
    public function testGoodDateIsInRange(): void
    {
        $date_range = new DateRange(now()->subDay(), now()->addDay());
        $date = new DateDummy();

        $this->assertTrue($date->dateIsInRangeDummy(now(), $date_range));
    }
}
