<?php

namespace Cronos\Logs\Tests;

use Cronos\Logs\Providers\LogsServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;
use Illuminate\Foundation\Application;
use ReflectionClass;

/**
 * Class TestCase
 *
 * @package Cronos\Logs\Tests
 * @author Johnny Charcosset <jcharcosset@pageup.fr>
 * @author Antoine TRAN <atran@pageup.fr>
 */
abstract class TestCase extends Orchestra
{
    /**
     * Set up
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->withFactories((__DIR__.'/factories'));
    }

    /**
     * Get package provider
     *
     * @param  Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [LogsServiceProvider::class];
    }

    /**
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
