<?php

namespace Cronos\Logs\Tests\Abstracts;

use Cronos\Logs\Exceptions\ExchangeInvalidFilenameException;
use Cronos\Logs\Models\Path;
use Cronos\Logs\Tests\Dummies\ExchangeDummy;
use Cronos\Logs\Tests\TestCase;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon as CarbonFacade;
use Mockery;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use ReflectionException;

/**
 * Class ExchangeAbstractTest
 *
 * @package Cronos\Logs\Tests\Abstracts
 * @coversDefaultClass Cronos\Logs\Abstracts\ExchangeAbstract
 */
class ExchangeAbstractTest extends TestCase
{
    /** @var vfsStreamDirectory */
    private vfsStreamDirectory $root;

    /**
     * @before
     * @codeCoverageIgnore
     */
    public function before(): void
    {
        $this->root = vfsStream::setup();
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithoutDir(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnFalse();

        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->get();

        $this->assertEquals(collect(), $result);
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithEmptyDir(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());

        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->get();

        $this->assertEquals(collect(), $result);
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithBadFilesInDir(): void
    {
        $file = vfsStream::newFile('bad-name', []);
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnFalse();

        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->get();

        $this->assertEquals(collect(), $result);
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithGoodFilesOutDateRangeInDir(): void
    {
        $file = vfsStream::newFile('api-'.now()->subDay()->format('Y-m-d').'.log', []);
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnTrue();

        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->get();

        $this->assertEquals(collect(), $result);
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithGoodFileWithBadFormat(): void
    {
        $file = vfsStream::newFile('api-'.now()->subDay()->format('d-d-d').'.log', []);
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());

        $exchange = new ExchangeDummy('myCustomType');

        $this->expectException(Exception::class);
        $result = $exchange->get();
    }

    /**
     * Test get
     *
     * @group abstracts
     * @covers ::get
     * @covers ::getContent
     * @covers ::contentLines
     * @covers ::__construct
     */
    public function testGetWithGoodFilesInDateRangeInDir(): void
    {
        $file = vfsStream::newFile('api-'.now()->format('Y-m-d').'.log', 775);
        $file->withContent('file_content');
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->with('myCustomType')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getFile')
            ->once()
            ->andReturn($file->url());

        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->get();

        $this->assertEquals(collect()->push((object) ['file_content']), $result);
    }

    /**
     * Test getCarbonInstanceFromFilename
     *
     * @group abstracts
     * @covers ::getCarbonInstanceFromFilename
     * @covers ::__construct
     * @throws ReflectionException
     */
    public function testGetCarbonInstanceFromFilenameException(): void
    {
        $exchange = new ExchangeDummy('myCustomType');

        $this->expectException(ExchangeInvalidFilenameException::class);
        $this->invokeMethod($exchange, 'getCarbonInstanceFromFilename', ['myFilename']);
    }

    /**
     * Test getCarbonInstanceFromFilename
     *
     * @group abstracts
     * @covers ::getCarbonInstanceFromFilename
     * @covers ::__construct
     * @throws ReflectionException
     */
    public function testGetCarbonInstanceFromFilename(): void
    {
        $file = vfsStream::newFile('api-'.now()->format('Y-m-d').'.log', 775);
        $file->withContent('file_content');
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnTrue();

        $exchange = new ExchangeDummy('myCustomType');
        $result = $this->invokeMethod($exchange, 'getCarbonInstanceFromFilename', ['custom-2021-09-01.log']);

        $this->assertEquals(CarbonFacade::createFromFormat('Y-m-d', '2021-09-01')->startOfDay(), $result);
    }

    /**
     * Test delete
     *
     * @group abstracts
     * @covers ::delete
     * @covers ::__construct
     */
    public function testDeleteWithInvalidFolder(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->once()
            ->andReturnFalse();

        $date = now();
        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->delete($date);

        $this->assertTrue($result);
    }

    /**
     * Test delete
     *
     * @group abstracts
     * @covers ::delete
     * @covers ::__construct
     */
    public function testDeleteWithValidFolderWithoutFiles(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());

        $date = now();
        $exchange = new ExchangeDummy('myCustomType');
        $result = $exchange->delete($date);

        $this->assertTrue($result);
    }

    /**
     * Test delete
     *
     * @group abstracts
     * @covers ::delete
     * @covers ::__construct
     */
    public function testDeleteWithValidFolderWithBadFiles(): void
    {
        $file = vfsStream::newFile('api.log', 775);
        $file->withContent('file_content');
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnFalse();
        $path->shouldReceive('getFile')
            ->once()
            ->andReturn($file->url());

        $date = now();
        $exchange = new ExchangeDummy('myCustomType');
        $this->expectException(ExchangeInvalidFilenameException::class);
        $exchange->delete($date);
    }

    /**
     * Test delete
     *
     * @group abstracts
     * @covers ::delete
     * @covers ::__construct
     * @throws ExchangeInvalidFilenameException
     */
    public function testDeleteWithValidFolderWithDirFiles(): void
    {
        $folder = vfsStream::newDirectory('api-'.now()->format('Y-m-d'), 777);
        $this->root->addChild($folder);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('getFile')
            ->once()
            ->andReturn($folder->url());

        $date = now();
        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->delete($date));
    }

    /**
     * Test delete
     *
     * @group abstracts
     * @covers ::delete
     * @covers ::__construct
     * @throws ExchangeInvalidFilenameException
     */
    public function testDeleteWithValidFolderWithFiles(): void
    {
        $file = vfsStream::newFile('api-'.now()->format('Y-m-d').'.log', 777);
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('directoryExists')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('getStorage')
            ->with('myCustomType')
            ->once()
            ->andReturn($this->root->url());
        $path->shouldReceive('getFile')
            ->once()
            ->andReturn($file->url());
        $path->shouldReceive('isValidFilename')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('deleteFile')
            ->once()
            ->andReturnTrue();

        $date = now();
        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->delete($date));
    }

    /**
     * Test save
     *
     * @group abstracts
     * @covers ::save
     * @covers ::__construct
     */
    public function testSaveWithoutParams(): void
    {
        $file = vfsStream::newFile('api-'.now()->format('Y-m-d').'.log', 777);
        $this->root->addChild($file);

        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('makeDirectory')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('appendFile')
            ->once()
            ->andReturnTrue();

        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->save());
    }

    /**
     * Test save
     *
     * @group abstracts
     * @covers ::save
     * @covers ::generateLine
     * @covers ::__construct
     */
    public function testSaveWithRequest(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('makeDirectory')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('appendFile')
            ->once()
            ->andReturnTrue();

        $request = Mockery::mock(Request::class)->makePartial();
        $request->shouldReceive('method')
            ->once()
            ->andReturn('get');
        $request->shouldReceive('path')
            ->once()
            ->andReturn('http://test.mock');
        $request->shouldReceive('all')
            ->once()
            ->andReturn([]);

        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->save($request));
    }

    /**
     * Test save
     *
     * @group abstracts
     * @covers ::save
     * @covers ::generateLine
     * @covers ::__construct
     */
    public function testSaveWithResponse(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('makeDirectory')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('appendFile')
            ->once()
            ->andReturnTrue();

        $response = Mockery::mock(Response::class)->makePartial();
        $response->shouldReceive('status')
            ->once()
            ->andReturn(204);

        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->save(null, $response));
    }

    /**
     * Test save
     *
     * @group abstracts
     * @covers ::save
     * @covers ::generateLine
     * @covers ::__construct
     */
    public function testSaveWithResponseWithRequest(): void
    {
        $path = Mockery::mock('alias:'.Path::class);
        $path->shouldReceive('makeDirectory')
            ->once()
            ->andReturnTrue();
        $path->shouldReceive('appendFile')
            ->once()
            ->andReturnTrue();

        $response = Mockery::mock(Response::class)->makePartial();
        $response->shouldReceive('status')
            ->once()
            ->andReturn(204);

        $request = Mockery::mock(Request::class)->makePartial();
        $request->shouldReceive('method')
            ->once()
            ->andReturn('get');
        $request->shouldReceive('path')
            ->once()
            ->andReturn('http://test.mock');
        $request->shouldReceive('all')
            ->once()
            ->andReturn([]);

        $exchange = new ExchangeDummy('myCustomType');

        $this->assertTrue($exchange->save($request, $response));
    }

    /**
     * Test payload
     *
     * @group abstracts
     * @covers ::payload
     * @covers ::__construct
     */
    public function testEmptyPayload(): void
    {
        $exchange = new ExchangeDummy('myCustomType');

        $this->assertEquals('[]', $this->invokeMethod($exchange, 'payload'));
    }

    /**
     * Test payload
     *
     * @group abstracts
     * @covers ::payload
     * @covers ::__construct
     */
    public function testExtraPayload(): void
    {
        config(['logs.max_payload_size' => 0.5]);

        $request = Mockery::mock(Request::class)->makePartial();
        $request->shouldReceive('all')
            ->once()
            ->andReturn([
                'message' => 'Mon message',
                'password' => 'secret_password',
                'too_long_message' => 'mon message beaucoup trop long',
            ]);

        $exchange = new ExchangeDummy('myCustomType');
        $this->assertEquals('{"message":"payload size is too large, check the maximum payload size authorized in the config file"}', $this->invokeMethod($exchange, 'payload', [$request]));
    }

    /**
     * Test payload
     *
     * @group abstracts
     * @covers ::payload
     * @covers ::__construct
     */
    public function testpayload(): void
    {
        $request = Mockery::mock(Request::class)->makePartial();
        $request->shouldReceive('all')
            ->once()
            ->andReturn([
                'message' => 'Mon message',
                'password' => 'secret_password',
            ]);

        $exchange = new ExchangeDummy('myCustomType');
        $this->assertEquals('{"message":"Mon message"}', $this->invokeMethod($exchange, 'payload', [$request]));
    }
}
