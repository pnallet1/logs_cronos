<?php

namespace Cronos\Logs\Tests\Models;

use Cronos\Logs\Models\Path;
use Cronos\Logs\Tests\TestCase;
use Illuminate\Support\Facades\File;
use Mockery;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

/**
 * Class PathTest
 *
 * @package Cronos\Logs\Tests\Models
 * @coversDefaultClass Cronos\Logs\Models\Path
 */
class PathTest extends TestCase
{
    /**
     * Test getStorage
     *
     * @group models
     * @covers ::getStorage
     */
    public function testGetStorage(): void
    {
        $expected_path = storage_path().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'myCustomType';

        $this->assertEquals($expected_path, Path::getStorage('myCustomType'));
    }

    /**
     * Test getFile
     *
     * @group models
     * @covers ::getFile
     */
    public function testGetFile(): void
    {
        $expected_path = storage_path().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'myCustomType'.DIRECTORY_SEPARATOR.'myFile';

        $this->assertEquals($expected_path, Path::getFile("myCustomType", "myFile"));
    }

    /**
     * Test makeDirectory
     *
     * @group models
     * @covers ::makeDirectory
     */
    public function testMakeDirectory(): void
    {
        $file = Mockery::mock('alias:'.File::class)->makePartial();
        $file->shouldReceive('makeDirectory')
            ->once()
            ->andReturnTrue();
        $this->assertTrue(Path::makeDirectory("myCustomType"));
    }

    /**
     * Test deleteFile
     *
     * @group models
     * @covers ::deleteFile
     */
    public function testDeleteFile(): void
    {
        $file = Mockery::mock('alias:'.File::class)->makePartial();
        $file->shouldReceive('delete')
            ->once()
            ->andReturnTrue();

        $this->assertTrue(Path::deleteFile("myCustomType", "myFile"));
    }

    /**
     * Test appendFile
     *
     * @group models
     * @covers ::appendFile
     */
    public function testAppendFile(): void
    {
        $file = Mockery::mock('alias:'.File::class)->makePartial();
        $file->shouldReceive('append')
            ->once()
            ->andReturnTrue();

        $this->assertTrue(Path::appendFile("myCustomType", "myFile", "{}"));
    }

    /**
     * Test directoryExists
     *
     * @group models
     * @covers ::directoryExists
     */
    public function testDirectoryNotExists(): void
    {
        $this->assertFalse(Path::directoryExists('myCustomType'));
    }

    /**
     * Test directoryExists
     *
     * @group models
     * @covers ::directoryExists
     */
    public function testDirectoryExists(): void
    {
        $file = Mockery::mock('alias:'.File::class);
        $file->shouldReceive('isDirectory')
            ->andReturn(true);

        $this->assertTrue(Path::directoryExists('myCustomType'));
    }

    /**
     * Test isValidFilename
     *
     * @group models
     * @covers ::isValidFilename
     */
    public function testIsValidFilenameWithBadFilename(): void
    {
        $this->assertFalse(Path::isValidFilename('myCustomType'));
    }

    /**
     * Test isValidFilename
     *
     * @group models
     * @covers ::isValidFilename
     */
    public function testIsValidFilenameWithBadName(): void
    {
        $this->assertFalse(Path::isValidFilename('log-2020.log'));
    }

    /**
     * Test isValidFilename
     *
     * @group models
     * @covers ::isValidFilename
     */
    public function testIsValidFilename(): void
    {
        $this->assertTrue(Path::isValidFilename('log-2020-01-01.log'));
    }
}
