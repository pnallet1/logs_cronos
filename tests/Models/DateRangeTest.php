<?php

namespace Cronos\Logs\Tests\Models;

use Carbon\Carbon;
use Cronos\Logs\Models\DateRange;
use Cronos\Logs\Tests\TestCase;

/**
 * Class DateRangeTest
 *
 * @package Cronos\Logs\Tests\Models
 * @coversDefaultClass Cronos\Logs\Models\DateRange
 */
class DateRangeTest extends TestCase
{
    /**
     * Test getStartDate
     *
     * @group models
     * @covers ::getStartDate
     */
    public function testGetStartDate(): void
    {
        $start_date_mocked = now();

        $dateRange = new DateRange($start_date_mocked);
        $start_date = $dateRange->getStartDate();

        $this->assertEquals($start_date_mocked, $start_date);
    }

    /**
     * Test getEndDate
     *
     * @group models
     * @covers ::getEndDate
     */
    public function testGetEndDate(): void
    {
        $end_date_mocked = now();

        $dateRange = new DateRange(now(), $end_date_mocked);
        $end_date = $dateRange->getEndDate();

        $this->assertEquals($end_date_mocked, $end_date);
    }

    /**
     * Test setStartDate
     *
     * @group models
     * @covers ::setStartDate
     */
    public function testSetStartDate(): void
    {
        $start_date_mocked = now();

        $dateRange = new DateRange();
        $dateRange->setStartDate($start_date_mocked);

        $this->assertEquals($start_date_mocked, $dateRange->getStartDate());
    }

    /**
     * Test setEndDate
     *
     * @group models
     * @covers ::setEndDate
     */
    public function testSetEndDate(): void
    {
        $end_date_mocked = now();

        $dateRange = new DateRange();
        $dateRange->setEndDate($end_date_mocked);

        $this->assertEquals($end_date_mocked, $dateRange->getEndDate());
    }

    /**
     * Test getStartOfDayStartDate
     *
     * @group models
     * @covers ::getStartOfDayStartDate
     */
    public function testGetStartOfDayStartDate(): void
    {
        $start_date_mocked = now();

        $dateRange = new DateRange($start_date_mocked);
        $start_date = $dateRange->getStartOfDayStartDate();

        $this->assertEquals($start_date_mocked->startOfDay(), $start_date);
    }

    /**
     * Test getStartOfDayEndDate
     *
     * @group models
     * @covers ::getStartOfDayEndDate
     */
    public function testGetStartOfDayEndDate(): void
    {
        $end_date_mocked = now();

        $dateRange = new DateRange($end_date_mocked);
        $end_date = $dateRange->getStartOfDayEndDate();

        $this->assertEquals($end_date_mocked->startOfDay(), $end_date);
    }

    /**
     * Test getStartDateFromFormat
     *
     * @group models
     * @covers ::getStartDateFromFormat
     */
    public function testGetStartDateFromFormat(): void
    {
        $start_date_mocked = now();

        $dateRange = new DateRange($start_date_mocked);
        $start_date = $dateRange->getStartDateFromFormat("Y-m-d");

        $this->assertEquals($start_date_mocked->format("Y-m-d"), $start_date);
    }

    /**
     * Test getEndDateFromFormat
     *
     * @group models
     * @covers ::getEndDateFromFormat
     */
    public function testGetEndDateFromFormat(): void
    {
        $end_date_mocked = now();

        $dateRange = new DateRange($end_date_mocked);
        $end_date = $dateRange->getEndDateFromFormat("Y-m-d");

        $this->assertEquals($end_date_mocked->format("Y-m-d"), $end_date);
    }

    /**
     * Test __construct
     *
     * @group models
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $start_date_mocked = now();
        $end_date_mocked = now()->addDay();

        $dateRange = new DateRange();
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getStartDate()
        );
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getEndDate()
        );

        $dateRange = new DateRange($start_date_mocked);
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getStartDate()
        );
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getEndDate()
        );

        $dateRange = new DateRange(null, $end_date_mocked);
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getStartDate()
        );
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getEndDate()
        );

        $dateRange = new DateRange($start_date_mocked, $end_date_mocked);
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getStartDate()
        );
        $this->assertInstanceOf(
            Carbon::class,
            $dateRange->getEndDate()
        );
    }
}
