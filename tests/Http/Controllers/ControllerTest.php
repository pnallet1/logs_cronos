<?php

namespace Cronos\Logs\Tests\Http\Controllers;

use Cronos\Logs\Http\Controllers\Controller;
use Cronos\Logs\Tests\TestCase;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class LogsControllerTest
 *
 * @package Cronos\Logs\Tests\Http\Controllers
 * @coversDefaultClass Cronos\Logs\Http\Controllers\Controller
 */
class ControllerTest extends TestCase
{
    /**
     * Test index
     *
     * @group controllers
     * @covers ::paginate
     */
    public function testPaginate()
    {
        $controller = new Controller();

        $pagination = $controller->paginate([]);

        $this->assertInstanceOf(LengthAwarePaginator::class, $pagination);
    }
}
