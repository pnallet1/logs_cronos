<?php

namespace Cronos\Logs\Tests\Http\Controllers;

use Cronos\Logs\Tests\TestCase;
use Illuminate\Http\JsonResponse;

/**
 * Class LogsControllerTest
 *
 * @package Cronos\Logs\Tests\Http\Controllers
 * @coversDefaultClass Cronos\Logs\Http\Controllers\LogsController
 */
class LogsControllerTest extends TestCase
{
    /**
     * Test index
     *
     * @group controllers
     * @covers ::index
     */
    public function testIndex()
    {
        $response = $this->call('GET', '/api/logs/');
        $response->assertStatus(200);

        $this->assertEquals(0, ini_get('max_execution_time'));
        $this->assertEquals(-1, ini_get('memory_limit'));

        $this->isInstanceOf(JsonResponse::class);
    }

    /**
     * Test destroy
     *
     * @group controllers
     * @covers ::destroy
     */
    public function testDestroyWithoutSuppressionDate()
    {
        $response = $this->call('DELETE', '/api/logs/');
        $response->assertStatus(204);

        $this->isInstanceOf(JsonResponse::class);
    }

    /**
     * Test destroy
     *
     * @group controllers
     * @covers ::destroy
     */
    public function testDestroyWithSuppressionDate()
    {
        $response = $this->call(
            'DELETE',
            '/api/logs/',
            ['suppression_date' => '2020-01-01']
        );
        $response->assertStatus(204);

        $this->isInstanceOf(JsonResponse::class);
    }
}
