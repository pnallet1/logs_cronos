<?php

namespace Cronos\Logs\Tests\Http\Middleware;

use Cronos\Logs\Http\Middleware\Exchange;
use Cronos\Logs\Tests\TestCase;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Mockery;

/**
 * Class LogsControllerTest
 *
 * @package Cronos\Logs\Tests\Http\Middleware
 * @coversDefaultClass Cronos\Logs\Http\Middleware\Exchange
 */
class ExchangeTest extends TestCase
{
    /**
     * Test handle
     *
     * @group middleware
     * @covers ::handle
     * @covers ::shouldBeLogged
     */
    public function testHandleWithoutRequestTagWithoutResponseTag()
    {
        $exchangeServiceMock = Mockery::mock(\Cronos\Logs\Services\Exchange::class)->makePartial();
        $request = Mockery::mock(Request::class)->makePartial();
        $header = Mockery::mock(ResponseHeaderBag::class)->makePartial();
        $response = Mockery::mock(JsonResponse::class)->makePartial();

        $request->shouldReceive('header')
            ->andReturn(false);
        $header->shouldReceive('get')
            ->andReturn(false);

        $response->headers = $header;

        $exchangeServiceMock->shouldNotReceive('save');

        $exchange = new Exchange($exchangeServiceMock);
        $exchange->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     * Test handle
     *
     * @group middleware
     * @covers ::handle
     * @covers ::shouldBeLogged
     */
    public function testHandleWithRequestTagWithoutResponseTag()
    {
        $exchangeServiceMock = Mockery::mock(\Cronos\Logs\Services\Exchange::class)->makePartial();
        $apiExchange = Mockery::mock(\Cronos\Logs\Services\Exchanges\ApiExchange::class)->makePartial();
        $request = Mockery::mock(Request::class)->makePartial();
        $header = Mockery::mock(ResponseHeaderBag::class)->makePartial();
        $response = Mockery::mock(JsonResponse::class)->makePartial();

        $request->shouldReceive('header')
            ->andReturn(true);
        $header->shouldReceive('get')
            ->andReturn(false);

        $response->headers = $header;

        $apiExchange->shouldReceive('save')->once();
        $exchangeServiceMock->shouldReceive('__invoke')->andReturn($apiExchange);

        $exchange = new Exchange($exchangeServiceMock);
        $exchange->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     * Test handle
     *
     * @group middleware
     * @covers ::handle
     * @covers ::shouldBeLogged
     */
    public function testHandleWithoutRequestTagWithResponseTag()
    {
        $exchangeServiceMock = Mockery::mock(\Cronos\Logs\Services\Exchange::class)->makePartial();
        $apiExchange = Mockery::mock(\Cronos\Logs\Services\Exchanges\ApiExchange::class)->makePartial();
        $request = Mockery::mock(Request::class)->makePartial();
        $header = Mockery::mock(ResponseHeaderBag::class)->makePartial();
        $response = Mockery::mock(JsonResponse::class)->makePartial();

        $request->shouldReceive('header')
            ->andReturn(false);
        $header->shouldReceive('get')
            ->andReturn(true);

        $response->headers = $header;

        $apiExchange->shouldReceive('save')->once();
        $exchangeServiceMock->shouldReceive('__invoke')->andReturn($apiExchange);

        $exchange = new Exchange($exchangeServiceMock);
        $exchange->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     * Test handle
     *
     * @group middleware
     * @covers ::handle
     * @covers ::shouldBeLogged
     */
    public function testHandleWithRequestTagWithResponseTag()
    {
        $exchangeServiceMock = Mockery::mock(\Cronos\Logs\Services\Exchange::class)->makePartial();
        $apiExchange = Mockery::mock(\Cronos\Logs\Services\Exchanges\ApiExchange::class)->makePartial();
        $request = Mockery::mock(Request::class)->makePartial();
        $header = Mockery::mock(ResponseHeaderBag::class)->makePartial();
        $response = Mockery::mock(JsonResponse::class)->makePartial();

        $request->shouldReceive('header')
            ->andReturn(true);
        $header->shouldReceive('get')
            ->andReturn(true);

        $response->headers = $header;

        $apiExchange->shouldReceive('save')->once();
        $exchangeServiceMock->shouldReceive('__invoke')->andReturn($apiExchange);

        $exchange = new Exchange($exchangeServiceMock);
        $exchange->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     * Test __construct
     *
     * @group middleware
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $exchangeServiceMock = Mockery::mock(\Cronos\Logs\Services\Exchange::class)->makePartial();
        $exchange = new Exchange($exchangeServiceMock);

        $this->assertInstanceOf(Exchange::class, $exchange);
    }
}
