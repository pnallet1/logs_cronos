<?php

namespace Cronos\Logs\Tests\Console\Commands;

use Cronos\Logs\Tests\TestCase;

/**
 * Class LogRotateTest
 *
 * @package Cronos\Logs\Tests\Console\Commands
 * @coversDefaultClass Cronos\Logs\Console\Commands\LogRotate
 */
class LogRotateTest extends TestCase
{
    /**
     * Test handle
     *
     * @group commands
     * @covers ::handle
     */
    public function testHandle()
    {
        $this->artisan('log:rotate')
        ->assertExitCode(true);
    }
}
