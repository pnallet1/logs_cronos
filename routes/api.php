<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('api/logs', 'Cronos\Logs\Http\Controllers\LogsController@index');
Route::delete('api/logs', 'Cronos\Logs\Http\Controllers\LogsController@destroy');
