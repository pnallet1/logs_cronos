<?php

declare(strict_types=1);

use Carbon\Carbon;

if (! function_exists('set_unlimited_memory')) {
    function set_unlimited_memory(): void
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }
}

if (! function_exists('create_date_from_format')) {
    function create_date_from_format($date): Carbon
    {
        return Carbon::createFromFormat('Y-m-d', $date);
    }
}
